package com.projectbeta.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.projectbeta.game.MainGame;
import com.projectbeta.handlers.GameStateManager;
import com.projectbeta.screens.menus.OptionsWindow;
import com.projectbeta.screens.menus.PlayPauseWindow;
import com.projectbeta.world.Level;
import com.projectbeta.world.LevelList;
import com.projectbeta.world.LevelRenderer;

/**
 * Created by Kevin Xiao on 2015-06-21.
 */
public class PlayScreen extends GameState {

    private boolean isPaused;
    private PlayPauseWindow pauseMenu;
    private OptionsWindow optionsMenu;
    private LevelRenderer levelRenderer;
    private LevelList levelManager;
    private OrthographicCamera levelCamera;

    public PlayScreen(MainGame game) {
        super(game);

        isPaused = false;

        // Set up window
        pauseMenu = new PlayPauseWindow();
        optionsMenu = OptionsWindow.getInstance();

        // Set size and position of pause menu container
        pauseMenu.setSize(stage.getWidth() / 1.5f, stage.getHeight() / 1.5f);

        stage.addActor(pauseMenu);
        stage.addActor(optionsMenu);

        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    if (pauseMenu.isVisible()) {
                        if (optionsMenu.isVisible()) {
                            optionsMenu.hide();
                        } else {
                            ((PlayScreen) GameStateManager.getInstance().getCurrentGameState()).resumeGame();
                        }
                    } else {
                        GameStateManager.getInstance().getCurrentGameState().pause();
                    }
                    return true;
                }
                return super.keyDown(event, keycode);
            }
        });

        levelManager = LevelList.getInstance();
        levelCamera = new OrthographicCamera();
        levelCamera.setToOrtho(false, 32, 18);
    }

    public void setLevel(TiledMap map) {
        levelManager.loadLevel(map);
        levelRenderer = new LevelRenderer(levelManager.getCurrentLevel(), levelManager.UNIT_SCALE);
        levelRenderer.setView(levelCamera);
    }

    @Override
    public void show() {
        assetHandler.loadGroup("test");
        assetHandler.finishLoading();

        pauseMenu.setModal(false);
        pauseMenu.setVisible(false);
        stage.addActor(optionsMenu);

        this.setLevel((TiledMap) assetHandler.get("levels/test/test.tmx"));
        super.show();
    }

    @Override
    public void update(float dt) {
        stage.act(dt);

        // Update entity information
        if (!isPaused) {

        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        // Clear screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        // Render level before anything
        levelRenderer.render();

        stage.draw();
        if (isPaused) {
            batch.begin();
            font.draw(batch, "Game is paused!", getXProportional(50), getYProportional(75));
            batch.end();
        }



        // Sprite Batch rendering
        batch.setProjectionMatrix(game.getHudCam().combined);
        batch.begin();
        font.draw(batch, "Play State: FPS " + Gdx.graphics.getFramesPerSecond(), getXProportional(50), getYProportional(50));
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        fontParameter.size = 16 * stage.getViewport().getScreenHeight() / MainGame.VID_HEIGHT;
        game.getHudCam().setToOrtho(false, stage.getViewport().getScreenWidth(), stage.getViewport().getScreenHeight());
        font = fontGenerator.generateFont(fontParameter);
    }

    @Override
    public void pause() {
        isPaused = true;
        pauseMenu.show();
    }

    @Override
    public void resume() {
        // Window is in focus again
        // Do nothing
    }

    public void resumeGame() {
        pauseMenu.hide();
        isPaused = false;
    }

    @Override
    public void hide() {
        pauseMenu.setModal(false);
        this.isPaused = false;
        optionsMenu.remove();
        super.hide();
    }

    @Override
    public void dispose() {
        assetHandler.unloadGroup("test");
    }


}
