package com.projectbeta.screens;

import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.projectbeta.game.MainGame;
import com.projectbeta.screens.menus.OptionsWindow;
import com.projectbeta.tween.ActorAccessor;

/**
 * Created by Kevin Xiao on 2015-06-21.
 */
public class MainMenuScreen extends GameState {

    private VisTable table;
    private final VisTextButton playBtn = new VisTextButton("Play");
    private final VisTextButton optionsBtn = new VisTextButton("Options");
    private final VisTextButton quitBtn = new VisTextButton("Quit");
    private OptionsWindow optionsWindow = OptionsWindow.getInstance();

    public MainMenuScreen(MainGame game) {
        super(game);

        // Set up table
        table = new VisTable();
        table.setFillParent(true);
        table.align(Align.center | Align.top);
        table.padTop(200);

        // Initialize buttons with event listeners
        this.initializeButtons();

        // Add buttons to the main menu container
        table.add(playBtn).size(150, 50).pad(10);
        table.row();
        table.add(optionsBtn).size(150, 50).pad(10);
        table.row();
        table.add(quitBtn).size(150, 50).pad(10);

        stage.addActor(table);
        stage.addActor(optionsWindow);

        optionsWindow.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    optionsWindow.hide();
                    return true;
                }
                return super.keyDown(event, keycode);
            }
        });
    }

    @Override
    public void show() {
        super.show();

        // Animate table
        table.setColor(1, 1, 1, 0);
        table.addAction(Actions.fadeIn(0.5f, Interpolation.fade));
        Tween.from(table, ActorAccessor.Y, .5f).target(Gdx.graphics.getHeight() / 12).start(tweenManager);
        stage.addActor(optionsWindow);
    }

    @Override
    public void update(float dt) {
        tweenManager.update(dt);
        stage.act(dt);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        // Clear screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        // Update stage
        stage.draw();
        batch.setProjectionMatrix(game.getHudCam().combined);
        // Sprite Batch rendering
        batch.begin();
        font.draw(batch, "Main Menu: FPS " + Gdx.graphics.getFramesPerSecond(), getXProportional(50), getYProportional(50));
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        fontParameter.size = 16 * stage.getViewport().getScreenHeight() / MainGame.VID_HEIGHT;
        game.getHudCam().setToOrtho(false, stage.getViewport().getScreenWidth(), stage.getViewport().getScreenHeight());
        font = fontGenerator.generateFont(fontParameter);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        super.hide();
        optionsWindow.remove();
    }

    @Override
    public void dispose() {

    }

    public void initializeButtons() {
        // Play Button changes state to Play
        this.playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gsm.setLoadScreen("test");
                super.clicked(event, x, y);
            }
        });

        // Options Button
        this.optionsBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                optionsWindow.show();
                super.clicked(event, x, y);
            }
        });

        // Quit button exits the game
        this.quitBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
                super.clicked(event, x, y);
            }
        });
    }
}
