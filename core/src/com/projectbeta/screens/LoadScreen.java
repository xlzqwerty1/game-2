package com.projectbeta.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.projectbeta.game.MainGame;
import com.projectbeta.handlers.GameStateManager;

/**
 * Created by Kevin Xiao on 2015-06-21.
 */
public class LoadScreen extends GameState {

    private boolean isLoaded = false;
    private float percentLoaded;
    private TextureAtlas loadingAtlas;
    private FreeTypeFontGenerator loadFontGenerator;
    private FreeTypeFontGenerator.FreeTypeFontParameter loadFontParameter;
    private BitmapFont loadFont;
    private VisLabel loadLabel;

    private VisImage loadingBar;
    private VisImage loadingFrame;

    public LoadScreen(MainGame game) {
        super(game);

        // Loading font
        loadFontGenerator = (FreeTypeFontGenerator) assetHandler.get("fonts/alterebo.ttf");
        loadFontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        loadFontParameter.size = 32;
        loadFont = loadFontGenerator.generateFont(loadFontParameter);
        loadLabel = new VisLabel("", new Label.LabelStyle(loadFont, Color.WHITE));
        loadLabel.setBounds(0, 0, 300, 50);
        loadLabel.setAlignment(Align.center);
        loadLabel.setPosition((stage.getWidth() - loadLabel.getWidth()) / 2, (stage.getHeight() - loadLabel.getHeight()) / 2);

        // Loading bar and frame
        loadingAtlas = (TextureAtlas) assetHandler.get("textures/menu/loadingbar/menu_loadingbar.pack");
        loadingBar = new VisImage(loadingAtlas.findRegion("loadingbar"));
        loadingFrame = new VisImage(loadingAtlas.findRegion("loadingframe"));
        loadingFrame.setPosition((stage.getWidth() - loadingFrame.getWidth()) / 2, (stage.getHeight() - loadingFrame.getHeight()) / 2);
        loadingBar.setPosition((stage.getWidth() - loadingFrame.getWidth()) / 2, (stage.getHeight() - loadingFrame.getHeight()) / 2);

        stage.addActor(loadingFrame);
        stage.addActor(loadingBar);
        stage.addActor(loadLabel);

        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (isLoaded) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(gsm.setState(GameStateManager.State.PLAY));
                    return true;
                }
                return true;
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isLoaded) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(gsm.setState(GameStateManager.State.PLAY));
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void show() {
        isLoaded = false;
        percentLoaded = 0;
        super.show();
    }

    @Override
    public void update(float dt) {
        if (assetHandler.update()) {
            isLoaded = true;
        }

        // Interpolate loading percentage
        percentLoaded = Interpolation.linear.apply(percentLoaded, assetHandler.getProgress(), 0.2f);
        loadingBar.setScale(percentLoaded, 1);
        loadLabel.setText("Loading... " + MathUtils.round(percentLoaded * 100) + "%");

        stage.act(dt);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        // Base font
        fontParameter.size = 16 * stage.getViewport().getScreenHeight() / MainGame.VID_HEIGHT;
        font = fontGenerator.generateFont(fontParameter);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        loadFont.dispose();
    }
}
