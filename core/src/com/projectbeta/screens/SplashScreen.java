package com.projectbeta.screens;

import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;
import com.projectbeta.game.MainGame;
import com.projectbeta.handlers.GameStateManager;
import com.projectbeta.tween.SpriteAccessor;

/**
 * Created by Kevin Xiao on 2015-06-21.
 */
public class SplashScreen extends GameState {

    private TextureAtlas splashAtlas;
    private Sprite splash;
    private Sprite libgdx;

    public SplashScreen(MainGame game) {
        super(game);

        // Preload the required assets before continuing
        assetHandler.loadGroup("splash");
        assetHandler.loadGroup("loading");
        assetHandler.finishLoading();

        // Set up splash screen
        splashAtlas = (TextureAtlas) assetHandler.get("textures/levels/screens/splash/splashscreen.pack");
        splash = splashAtlas.createSprite("splash");
        libgdx = splashAtlas.createSprite("libgdx");

        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ESCAPE) {
                    switchToMainMenu();
                    return true;
                }
                return super.keyDown(event, keycode);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                switchToMainMenu();
                return true;
            }
        });
    }

    @Override
    public void show() {
        super.show();

        // Set up sprite positions
        splash.setCenter(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        splash.setOriginCenter();
        libgdx.setOrigin(0, 0);
        libgdx.setScale(0.5f);
        libgdx.setPosition(Gdx.graphics.getWidth() - (libgdx.getWidth() * libgdx.getScaleX()), 0);

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                switchToMainMenu();
            }
        }, 4f);

        // Animate splash
        Tween.set(splash, SpriteAccessor.ALPHA).target(0).start(tweenManager);
        Tween.set(libgdx, SpriteAccessor.ALPHA).target(0).start(tweenManager);
        Tween.to(splash, SpriteAccessor.ALPHA, 1.25f).target(1).repeatYoyo(1, .5f).start(tweenManager);
        Tween.to(libgdx, SpriteAccessor.ALPHA, 1.25f).target(1).repeatYoyo(1, .5f).start(tweenManager);
    }

    @Override
    public void update(float dt) {
        tweenManager.update(dt);
        stage.act(dt);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        splash.draw(batch);
        libgdx.draw(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        fontParameter.size = 16 * stage.getViewport().getScreenHeight() / MainGame.VID_HEIGHT;
        font = fontGenerator.generateFont(fontParameter);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        super.hide();
        assetHandler.unloadGroup("splash");
    }

    @Override
    public void dispose() {
        splashAtlas.dispose();
    }

    private void switchToMainMenu() {
        ((Game) Gdx.app.getApplicationListener()).setScreen(gsm.setState(GameStateManager.State.MENU));
        Timer.instance().clear();
    }
}
