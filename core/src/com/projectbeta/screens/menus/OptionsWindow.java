package com.projectbeta.screens.menus;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.projectbeta.handlers.GameStateManager;

/**
 * Created by Kevin Xiao on 2015-06-25.
 */
public class OptionsWindow extends WindowMenu {

    private final VisCheckBox debugCheck = new VisCheckBox("Debug Mode", false);

    private static OptionsWindow ourInstance = new OptionsWindow();

    public static OptionsWindow getInstance() {
        return ourInstance;
    }

    private OptionsWindow() {
        super("Options");

        this.addHideButton();
        this.getTitleLabel().setAlignment(Align.center);
        this.centerWindow();
        this.setMovable(false);

        this.add(debugCheck);

        debugCheck.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GameStateManager.getInstance().setDebug(debugCheck.isChecked());
            }
        });
    }

    public VisCheckBox getDebugCheck() {
        return debugCheck;
    }

}
