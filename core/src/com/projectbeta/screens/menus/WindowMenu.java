package com.projectbeta.screens.menus;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisWindow;

/**
 * Created by Kevin Xiao on 2015-06-25.
 */
public class WindowMenu extends VisWindow {

    public WindowMenu(String windowName) {
        super(windowName);

        this.setModal(false);
        this.setVisible(false);
        this.setColor(1, 1, 1, 0);
    }

    public void show() {
        this.setColor(1, 1, 1, 0);
        addAction(Actions.sequence(
                Actions.show(),
                Actions.fadeIn(FADE_TIME, Interpolation.fade)
        ));
        this.setModal(true);
    }

    public void hide() {
        addAction(Actions.sequence(
                Actions.fadeOut(FADE_TIME, Interpolation.fade),
                Actions.hide()
        ));
        this.setModal(false);
    }

    /**
     * Hides the window instead of removing it from the stage
     **/
    public void addHideButton() {
        VisImageButton closeButton = new VisImageButton("close-window");
        getTitleTable().add(closeButton).padRight(-getPadRight() + 0.7f);
        closeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                hide();
            }
        });

        if (VisUI.getDefaultTitleAlign() == Align.center && getTitleTable().getChildren().size == 2)
            getTitleTable().getCell(getTitleLabel()).padLeft(closeButton.getWidth() * 2);
    }
}
