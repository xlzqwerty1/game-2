package com.projectbeta.screens.menus;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.projectbeta.handlers.GameStateManager;
import com.projectbeta.screens.PlayScreen;

/**
 * Created by Kevin Xiao on 2015-06-25.
 */
public class PlayPauseWindow extends WindowMenu {

    private final VisTextButton resumeBtn = new VisTextButton("Resume");
    private final VisTextButton mainMenuBtn = new VisTextButton("Main Menu");
    private final VisTextButton optionsBtn = new VisTextButton("Options");

    public PlayPauseWindow() {
        super("Game Paused...");

        // Add buttons to the main menu container
        this.add(resumeBtn).size(150, 50).pad(15);
        this.row();
        this.add(optionsBtn).size(150, 50).pad(15);
        this.row();
        this.add(mainMenuBtn).size(150, 50).pad(15);
        this.getTitleLabel().setAlignment(Align.center);
        this.centerWindow();
        this.setMovable(false);

        this.initializeButtons();
    }

    public void initializeButtons() {
        // Resume button resumes game
        this.resumeBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((PlayScreen) GameStateManager.getInstance().getCurrentGameState()).resumeGame();
                super.clicked(event, x, y);
            }
        });

        // Options button displays the options window
        this.optionsBtn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                OptionsWindow.getInstance().show();
                OptionsWindow.getInstance().toFront();
                super.clicked(event, x, y);
            }
        });

        // Returns to main menu
        this.mainMenuBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(GameStateManager.getInstance().setState(GameStateManager.State.MENU));
                super.clicked(event, x, y);
            }
        });
    }

}
