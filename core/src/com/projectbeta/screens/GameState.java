package com.projectbeta.screens;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.projectbeta.game.MainGame;
import com.projectbeta.handlers.AssetHandler;
import com.projectbeta.handlers.GameStateManager;
import com.projectbeta.handlers.InputListenerUI;
import com.projectbeta.tween.ActorAccessor;
import com.projectbeta.tween.SpriteAccessor;

/**
 * GameState
 * abstract state class that all game states must extend
 * Created by Kevin Xiao on 2015-06-18.
 */
public abstract class GameState implements Screen {

    protected MainGame game;
    protected InputMultiplexer multiplexer;
    protected SpriteBatch batch;
    protected Stage stage;
    protected BitmapFont font;
    protected FreeTypeFontGenerator fontGenerator;
    protected FreeTypeFontGenerator.FreeTypeFontParameter fontParameter;
    protected TweenManager tweenManager;
    protected AssetHandler assetHandler;
    protected GameStateManager gsm;

    public GameState(MainGame game) {
        this.game = game;
        this.multiplexer = game.getMultiplexer();
        this.batch = game.getSpriteBatch();
        this.font = game.getFont();
        this.fontGenerator = game.getFontGenerator();
        this.fontParameter = game.getFontParameter();
        this.assetHandler = AssetHandler.getInstance();
        this.gsm = GameStateManager.getInstance();

        // Animating actors
        this.tweenManager = new TweenManager();
        Tween.registerAccessor(Actor.class, new ActorAccessor());
        Tween.registerAccessor(Sprite.class, new SpriteAccessor());

        this.stage = new Stage(new FitViewport(MainGame.VID_WIDTH, MainGame.VID_HEIGHT));
        this.stage.addListener(InputListenerUI.getInstance());
        this.stage.setDebugAll(gsm.getDebug());
    }

    public abstract void update(float dt);

    @Override
    public void show() {
        multiplexer.addProcessor(0, stage);
    }

    @Override
    public void hide() {
        multiplexer.removeProcessor(stage);
    }

    @Override
    public void render(float delta) {
        this.update(delta);
    }

    // Returns the x position proportional to current screen size over the default resolution
    public float getXProportional(float x) {
        return x * stage.getViewport().getScreenWidth() / MainGame.VID_WIDTH;
    }

    // Returns the y position proportional to current screen size over the default resolution
    public float getYProportional(float y) {
        return y * stage.getViewport().getScreenHeight() / MainGame.VID_HEIGHT;
    }

    public Stage getStage() {
        return stage;
    }
}
