package com.projectbeta.world;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Logger;

/**
 * Created by Kevin Xiao on 2015-06-26.
 */
public class LevelList implements Disposable {

    private Array<Level> levels;
    private Level currentLevel;
    public static float UNIT_SCALE = 1/32f;
    private Logger logger;

    private static LevelList ourInstance = new LevelList();
    public static LevelList getInstance() {
        return ourInstance;
    }

    private LevelList() {
        levels = new Array<Level>();
        logger = new Logger("LevelList", Logger.DEBUG);
    }

    public void loadLevel(TiledMap map) {
        boolean levelExists = false;
        for (Level level : levels) {
            if (level.getMap().equals(map)) {
                levelExists = true;
                currentLevel = level;
                logger.debug("Map already exists in levels array. Setting current map to this map.");
                break;
            }
        }
        if (!levelExists) {
            currentLevel = addLevel(new Level(map));
            logger.debug("Map does not exist in levels array. Adding map to levels array.");
        }
    }

    private Level addLevel(Level level) {
        levels.add(level);
        return level;
    }

    @Override
    public void dispose() {
        for (Level current : levels) {
            current.dispose();
        }
        logger.debug("shutting down");
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }
}
