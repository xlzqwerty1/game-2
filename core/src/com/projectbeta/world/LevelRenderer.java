package com.projectbeta.world;

import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * Created by Kevin Xiao on 2015-06-26.
 */
public class LevelRenderer extends OrthogonalTiledMapRenderer {

    private Level currentLevel;

    public LevelRenderer(Level level, float unitScale) {
        super(level.getMap(), unitScale);
        currentLevel = level;
    }

}
