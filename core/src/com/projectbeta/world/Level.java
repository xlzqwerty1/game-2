package com.projectbeta.world;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Disposable;
import com.projectbeta.handlers.AssetHandler;

/**
 * Created by Kevin Xiao on 2015-06-26.
 * Level stuff
 */
public class Level implements Disposable {

    private String levelName;
    private TiledMap map;

    public Level(TiledMap map) {
        this.map = map;
    }

    public TiledMap getMap() {
        return map;
    }

    @Override
    public void dispose() {
        //AssetHandler.getInstance().unloadGroup(levelName);
        map.dispose();
    }
}
