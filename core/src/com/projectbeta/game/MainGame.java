package com.projectbeta.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kotcrab.vis.ui.VisUI;
import com.projectbeta.handlers.AssetHandler;
import com.projectbeta.handlers.GameStateManager;
import com.projectbeta.handlers.InputProcessorGame;

public class MainGame extends Game {

    public static final String TITLE = "Project Beta";
    public static final int VID_WIDTH = 800, VID_HEIGHT = 450;

    private GameStateManager gsm;
    private OrthographicCamera hudCam;
    private InputMultiplexer multiplexer;
    private SpriteBatch batch;
    private AssetHandler assetHandler;

    private static ShapeRenderer debugUI;

    private BitmapFont font;
    private FreeTypeFontGenerator fontGenerator;
    private FreeTypeFontGenerator.FreeTypeFontParameter fontParameter;

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        // Load base resource files
        assetHandler = AssetHandler.getInstance();
        assetHandler.loadGroup("base");
        assetHandler.finishLoading();

        batch = new SpriteBatch();
        hudCam = new OrthographicCamera();
        hudCam.setToOrtho(false, VID_WIDTH, VID_HEIGHT);
        debugUI = new ShapeRenderer();
        VisUI.load();
        multiplexer = new InputMultiplexer();

        // Set up game state/screen manager
        gsm = GameStateManager.getInstance();
        gsm.setGame(this);

        // Set up on-the-fly font generation for screen resizing
        fontGenerator = (FreeTypeFontGenerator) assetHandler.get("fonts/droidsansmono.ttf");
        fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.borderColor = Color.WHITE;
        fontParameter.borderWidth = 0.3f;
        font = fontGenerator.generateFont(fontParameter);

        // Android stuff
        Gdx.input.setCatchBackKey(true);

        // Set up input management
        multiplexer.addProcessor(InputProcessorGame.getInstance());
        Gdx.input.setInputProcessor(multiplexer);

        // On game creation, switch to this screen first
        this.setScreen(gsm.setState(GameStateManager.State.SPLASH));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        VisUI.dispose();
        gsm.dispose();
        assetHandler.dispose();
    }

    public SpriteBatch getSpriteBatch() {
        return batch;
    }

    public BitmapFont getFont() {
        return font;
    }

    public FreeTypeFontGenerator getFontGenerator() {
        return fontGenerator;
    }

    public FreeTypeFontGenerator.FreeTypeFontParameter getFontParameter() {
        return fontParameter;
    }

    public static ShapeRenderer getDebugUI() {
        return debugUI;
    }

    public InputMultiplexer getMultiplexer() {
        return multiplexer;
    }

    public OrthographicCamera getHudCam() {
        return hudCam;
    }

}
