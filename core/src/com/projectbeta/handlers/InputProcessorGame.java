package com.projectbeta.handlers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * InputProcessorGame
 * handles game based input (different from UI)
 * Created by Kevin Xiao on 2015-06-18.
 */
public class InputProcessorGame implements InputProcessor {

    private GameStateManager gsm = GameStateManager.getInstance();

    //  Begin singleton declaration
    private static InputProcessorGame ourInstance = new InputProcessorGame();

    public static InputProcessorGame getInstance() {
        return ourInstance;
    }

    private InputProcessorGame() {
    }
    //  End singleton declaration

    @Override
    public boolean keyDown(int keycode) {
        switch (gsm.getCurrentState()) {
            case PLAY:
                break;
            case LOAD:
                break;
            case MENU:
                break;
            default:
                return false;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
