package com.projectbeta.handlers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

/**
 * Created by Kevin Xiao on 2015-06-19.
 */
public class InputListenerUI extends InputListener {

    private GameStateManager gsm = GameStateManager.getInstance();
    private boolean isLeftAlt = false;
    private boolean isLeftCtrl = false;
    private boolean isLeftShift = false;
    private int currentScreenWidth = Gdx.graphics.getWidth();
    private int currentScreenHeight = Gdx.graphics.getHeight();

    //  Begin singleton declaration
    private static InputListenerUI ourInstance = new InputListenerUI();

    public static InputListenerUI getInstance() {
        return ourInstance;
    }

    private InputListenerUI() {
    }
    //  End singleton declaration

    // DEBUGGING PURPOSES ONLY! To check coordinates of mouse clicks
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        Gdx.app.log("Cursor position", Gdx.input.getX() + ", " + (Gdx.graphics.getHeight() - Gdx.input.getY()));
        return false;
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        // Modifier keys (Shift | Ctrl | Alt)
        if (keycode == Input.Keys.ALT_LEFT) {
            isLeftAlt = true;
            return true;
        }
        if (keycode == Input.Keys.SHIFT_LEFT) {
            isLeftShift = true;
            return true;
        }
        if (keycode == Input.Keys.CONTROL_LEFT) {
            isLeftCtrl = true;
            return true;
        }

        // FULL SCREEN OPTION DESKTOP ONLY
        if (Gdx.app.getType() == Application.ApplicationType.Desktop && keycode == Input.Keys.ENTER && isLeftAlt) {
            if (!Gdx.graphics.isFullscreen()) {
                currentScreenWidth = Gdx.graphics.getWidth();
                currentScreenHeight = Gdx.graphics.getHeight();
                Gdx.graphics.setDisplayMode(Gdx.graphics.getDesktopDisplayMode().width, Gdx.graphics.getDesktopDisplayMode().height, true);
            } else {
                Gdx.graphics.setDisplayMode(currentScreenWidth, currentScreenHeight, false);
            }
            return true;
        }
        if (keycode == Input.Keys.F1) {
            gsm.setDebug(!gsm.getDebug());
        }
        return false;
    }

    @Override
    public boolean keyUp(InputEvent event, int keycode) {
        if (keycode == Input.Keys.ALT_LEFT) {
            isLeftAlt = false;
            return true;
        }
        if (keycode == Input.Keys.SHIFT_LEFT) {
            isLeftShift = false;
            return true;
        }
        if (keycode == Input.Keys.CONTROL_LEFT) {
            isLeftCtrl = false;
            return true;
        }
        return false;
    }
}
