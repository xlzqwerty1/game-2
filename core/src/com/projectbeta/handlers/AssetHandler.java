package com.projectbeta.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.*;

/**
 * Created by Kevin Xiao on 2015-06-24.
 */
public class AssetHandler implements Disposable, AssetErrorListener {

    private static final String TAG = "AssetManager";
    private static AssetHandler ourInstance = new AssetHandler();
    private AssetManager manager;
    private Logger logger;

    private ObjectMap<String, Array<AssetDescriptor>> groupMap = new ObjectMap<String, Array<AssetDescriptor>>();

    public static AssetHandler getInstance() {
        return ourInstance;
    }

    private AssetHandler() {
        logger = new Logger(TAG, Logger.DEBUG);
        manager = new AssetManager();
        manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(new InternalFileHandleResolver()));
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(new InternalFileHandleResolver()));
        manager.setErrorListener(this);
        init();
    }

    private void init() {
        FileHandle dirHandle = Gdx.files.internal("assets");

        for (FileHandle entry: dirHandle.list()) {
            readGroup(entry.toString());
        }
//        readGroup("assets/base.assets");
//        readGroup("assets/mainmenu.assets");
//        readGroup("assets/splash.assets");
//        readGroup("assets/test.assets");
    }

    public void loadGroup(String group) {
        logger.debug("Loading assets from group: " + group);

        Array<AssetDescriptor> assets = groupMap.get(group);

        if (assets != null) {
            for (AssetDescriptor asset : assets) {
                manager.load(asset);
            }
        } else {
            logger.error("Error loading group " + group + ", group not found!");
        }
    }

    public void unloadGroup(String group) {
        logger.debug("Unloading assets from group: " + group);

        Array<AssetDescriptor> assets = groupMap.get(group);

        if (assets != null) {
            for (AssetDescriptor asset : assets) {
                manager.unload(asset.fileName);
            }
        } else {
            logger.error("Error unloading group " + group + ", group not found!");
        }
    }

    public float getProgress() {
        return manager.getProgress();
    }

    public boolean update() {
        return manager.update();
    }

    private void readGroup(String groupFile) {
        logger.info("Loading group file: " + groupFile);

        try {
            XmlReader reader = new XmlReader();
            XmlReader.Element group = reader.parse(Gdx.files.internal(groupFile)).getChildByName("group");
            String groupName = group.getAttribute("name");
            if (groupMap.containsKey(groupName)) {
                logger.error("group " + groupName + " already exists, skipping");
            } else {
                logger.info("Registering group: " + groupName);
                Array<AssetDescriptor> assets = new Array<AssetDescriptor>();

                for (XmlReader.Element assetElement : group.getChildrenByName("asset")) {
                    logger.info("Registering file: " + assetElement.getAttribute("path") + " of type " + Class.forName(assetElement.getAttribute("type")).getSimpleName() + " in group " + groupName);
                    assets.add(new AssetDescriptor(assetElement.getAttribute("path"), Class.forName(assetElement.getAttribute("type"))));
                }
                groupMap.put(groupName, assets);
            }
        } catch (Exception e) {
            logger.error("Error loading group file: " + groupFile + " " + e.getMessage());
        }
    }

    public void finishLoading() {
        manager.finishLoading();
    }

    public synchronized Object get(String fileName) {
        if (manager.isLoaded(fileName)) {
            return manager.get(fileName);
        } else {
            logger.error("Could not get " + fileName + " from asset manager!");
            return null;
        }
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        logger.error("Error loading file: " + asset.fileName + " " + asset.type);
    }

    @Override
    public void dispose() {
        logger.debug("Logger shutting down.");
        manager.dispose();
    }
}
