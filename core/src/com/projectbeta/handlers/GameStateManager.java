package com.projectbeta.handlers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.projectbeta.game.MainGame;
import com.projectbeta.screens.*;
import com.projectbeta.screens.menus.OptionsWindow;
import com.projectbeta.world.LevelList;

/**
 * GameStateManager
 * singleton GSM to manage all available GameStates
 * Created by Kevin Xiao on 2015-06-18.
 */
public class GameStateManager {

    private static GameState currentGameState;
    private static State currentState;
    private SplashScreen splashScreen;
    private MainMenuScreen mainMenuScreen;
    private LoadScreen loadScreen;
    private PlayScreen playScreen;
    private boolean debugMode = false;
    private LevelList levelManager;

    private MainGame game;

    //  Begin singleton declaration
    private static GameStateManager ourInstance = new GameStateManager();

    public static GameStateManager getInstance() {
        return ourInstance;
    }

    private GameStateManager() {
        levelManager = LevelList.getInstance();
    }
    //  End singleton declaration

    public enum State {
        PLAY, LOAD, MENU, SPLASH
    }

    public void setGame(MainGame game) {
        this.game = game;
    }

    public GameState setState(State state) {
        if (state == State.PLAY) {
            if (playScreen == null) {
                playScreen = new PlayScreen(game);
            }
            currentGameState = playScreen;
            currentState = State.PLAY;
        } else if (state == State.LOAD) {
            if (loadScreen == null) {
                loadScreen = new LoadScreen(game);
            }
            currentGameState = loadScreen;
            currentState = State.LOAD;
        } else if (state == State.MENU) {
            if (mainMenuScreen == null) {
                mainMenuScreen = new MainMenuScreen(game);
            }
            currentGameState = mainMenuScreen;
            currentState = State.MENU;
        } else if (state == State.SPLASH) {
            if (splashScreen == null) {
                splashScreen = new SplashScreen(game);
            }
            currentGameState = splashScreen;
            currentState = State.SPLASH;
        }
        Gdx.app.debug("GSM", "Switching to " + state.toString() + " screen");
        return currentGameState;
    }

    public GameState getCurrentGameState() {
        return currentGameState;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void dispose() {
        if (playScreen != null) {
            playScreen.dispose();
        }
        if (loadScreen != null) {
            loadScreen.dispose();
        }
        if (mainMenuScreen != null) {
            mainMenuScreen.dispose();
        }
        if (splashScreen != null) {
            splashScreen.dispose();
        }
        Gdx.app.debug("GSM", "shutting down.");
        levelManager.dispose();
    }

    public void setDebug(boolean debug) {
        debugMode = debug;
        OptionsWindow.getInstance().getDebugCheck().setChecked(debug);
        if (playScreen != null) {
            playScreen.getStage().setDebugAll(debugMode);
        }
        if (loadScreen != null) {
            loadScreen.getStage().setDebugAll(debugMode);
        }
        if (mainMenuScreen != null) {
            mainMenuScreen.getStage().setDebugAll(debugMode);
        }
        if (splashScreen != null) {
            splashScreen.getStage().setDebugAll(debugMode);
        }
        Gdx.app.setLogLevel(debugMode ? Application.LOG_DEBUG : Application.LOG_NONE);
    }

    public boolean getDebug() {
        return debugMode;
    }

    public void setLoadScreen(String... groups) {
        for (String group : groups) {
            AssetHandler.getInstance().loadGroup(group);
        }
        ((Game) Gdx.app.getApplicationListener()).setScreen(this.setState(GameStateManager.State.LOAD));
    }
}
