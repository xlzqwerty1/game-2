package com.projectbeta.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.projectbeta.game.MainGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = MainGame.TITLE;
		config.height = MainGame.VID_HEIGHT;
		config.width = MainGame.VID_WIDTH;
		config.addIcon("icons/icon-128.png", Files.FileType.Internal);
		config.addIcon("icons/icon-32.png", Files.FileType.Internal);
		config.addIcon("icons/icon-16.png", Files.FileType.Internal);

		config.useGL30 = true;

		new LwjglApplication(new MainGame(), config);
	}
}